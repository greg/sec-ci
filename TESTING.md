The following community projects are intentionally "vulnerable" and good for testing code scanners.
### Go

https://github.com/Contrast-Security-OSS/go-test-bench.git
https://github.com/0c34/govwa.git
https://github.com/sqreen/go-dvwa.git

### Javascript

https://github.com/juice-shop/juice-shop.git
https://github.com/bkimminich/juice-shop.git
https://github.com/appsecco/dvna.git

### Rails

https://github.com/OWASP/railsgoat.git
https://github.com/logicalhacking/DVGM.git

### PHP

https://github.com/digininja/DVWA.git
https://github.com/Yavuzlar/VulnLab.git
https://github.com/RamadhanAmizudin/lazyweb.git

### Python

https://github.com/Significant-Gravitas/Auto-GPT.git
https://github.com/mattvaldes/vulnerable-api.git
https://github.com/sec-zone/vuln_app.git
https://github.com/kiwicom/xssable.git
https://github.com/adeyosemanputra/pygoat.git

### Secrets

https://github.com/trufflesecurity/test_keys.git
https://gitlab.com/gre9/secrets.git
https://github.com/Plazmaz/leaky-repo.git
https://github.com/OWASP/wrongsecrets.git

### Other

https://github.com/cider-security-research/cicd-goat.git
https://github.com/vulhub/vulhub.git
https://github.com/digininja/authlab.git
https://github.com/bridgecrewio/terragoat.git