#!/bin/bash
# This script clones a git repository, modifies its Dockerfile, builds a Docker image and pushes it to a registry

# Exit script on any error
set -e
set -o pipefail

# Define cleanup procedure
cleanup() {
    echo "Cleaning up temporary directory..."
    rm -rf "$TEMP_DIR"
}

# Trap the cleanup function to be called on exit
trap cleanup EXIT

# Define the temp directory
TEMP_DIR=$(mktemp -d)

# Clone the git repository to the temp directory
echo "Cloning the git repository..."
git clone https://github.com/jeremylong/DependencyCheck.git "$TEMP_DIR"

# Change directory to the cloned repository
cd "$TEMP_DIR"

# Rename the existing Dockerfile
echo "Renaming the existing Dockerfile..."
mv Dockerfile Dockerfile.old

# Download the new Dockerfile
echo "Downloading the new Dockerfile..."
curl -LOs https://gitlab.com/greg/sec-ci/-/raw/main/dependency-check/Dockerfile

# Build the Docker image
echo "Building the Docker image..."
docker build -t registry.gitlab.com/greg/sec-ci/dependency-check .

# Push the Docker image to the registry
echo "Pushing the Docker image to the registry..."
docker push registry.gitlab.com/greg/sec-ci/dependency-check

# Print a success message
echo "Done!"
