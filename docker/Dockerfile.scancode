# https://github.com/nexB/scancode-toolkit
# https://scancode-toolkit.readthedocs.io/en/latest/cli-reference/index.html
# Apache 2.0 License https://github.com/nexB/scancode-toolkit/blob/develop/LICENSE
FROM python:3.12-slim-bookworm

ENV DEBIAN_FRONTEND=noninteractive \
    PYTHON_VERSION=3.12 \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1


# OS requirements as per
# https://scancode-toolkit.readthedocs.io/en/latest/getting-started/install.html
RUN apt-get update \
 && apt-get install -y --no-install-recommends \
       bzip2 \
       curl \
       ca-certificates \
       xz-utils \
       zlib1g \
       libxml2-dev \
       libxslt1-dev \
       libgomp1 \
       libsqlite3-0 \
       libgcrypt20 \
       libpopt0 \
       libzstd1 \
       && apt-get clean \
       && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN export SCANCODE_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/nexB/scancode-toolkit/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/'| sed -e 's/v//g') && \
    curl -LO -s "https://github.com/nexB/scancode-toolkit/releases/download/v${SCANCODE_VERSION}/scancode-toolkit-v${SCANCODE_VERSION}_py${PYTHON_VERSION}-linux.tar.gz" && \
    tar -xzf "scancode-toolkit-v${SCANCODE_VERSION}_py${PYTHON_VERSION}-linux.tar.gz" && \
    rm "scancode-toolkit-v${SCANCODE_VERSION}_py${PYTHON_VERSION}-linux.tar.gz" && \
    mv "scancode-toolkit-v${SCANCODE_VERSION}" scancode && \
    cd scancode && \
    ./scancode --help

WORKDIR /scancode

CMD ["./scancode", "--help"]
