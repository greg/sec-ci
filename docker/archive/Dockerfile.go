FROM golang:1.22-bookworm

RUN go install honnef.co/go/tools/cmd/staticcheck@latest && \
    go install github.com/praetorian-inc/gokart@latest && \
    go install github.com/securego/gosec/v2/cmd/gosec@latest && \
    go install golang.org/x/vuln/cmd/govulncheck@latest
