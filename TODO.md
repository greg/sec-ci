Stuff to look into:

https://github.com/afdesk/trivy-html

# gitlab deprecated scanners

- bandit
- brakeman
- bundler-audit
- kubesec
- gosec-sast
- mobsf
- npm-audit
- phpcs-security-audit
- security-code-scan
- retirejs
- grype

# other tools:

- https://github.com/goodwithtech/dockle
- https://github.com/devops-kung-fu/bomber: Scans Software Bill of Materials (SBOMs) for security vulnerabilities
- https://github.com/ossf/criticality_score: Gives criticality score for an open source project
- https://github.com/mschwager/route-detect: Find authentication (authn) and authorization (authz) security bugs in web application routes.
- https://github.com/docker/scan-cli-plugin: Docker Scan is a Command Line Interface to run vulnerability detection on your Dockerfiles and Docker images
- https://github.com/docker/docker-bench-security: The Docker Bench for Security is a script that checks for dozens of common best-practices around deploying Docker containers in production.
- https://github.com/ronin-rb/ronin: Ronin is a Free and Open Source Ruby Toolkit for Security Research and Development. https://ronin-rb.dev/
- https://github.com/google/oss-fuzz
- https://github.com/chaoss/augur
- https://github.com/rubysec/bundler-audit
- https://github.com/flyerhzm/rails_best_practices
- https://github.com/XmirrorSecurity/OpenSCA-cli: OpenSCA is a Software Composition Analysis (SCA) solution that supports detection of open source component dependencies and vulnerabilities. https://opensca.xmirror.cn/
- https://github.com/apiaryio/curl-trace-parser
- https://github.com/goharbor/harbor
- https://github.com/guacsec/guac
- https://diffoscope.org/
- https://github.com/AppThreat/blint
- https://github.com/danielmiessler/SecLists: Collection of multiple types of lists used during security assessments, collected in one place.
- https://owasp.org/www-community/Source_Code_Analysis_Tools
- https://inside.caratlane.com/dependency-check-for-node-js-and-ruby-on-rails-177f269fffde
