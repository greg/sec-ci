# Sec-CI

Security Scanners in GitLab CI is `sec-ci`.

## What is Sec-CI?

Open Source Security Scanners in GitLab CI.

Note: This is a work in progress and not an official GitLab product. Use at your own risk.

## Usage

### GitLab CI

1. Locate the template you want to use in the `ci` directory.
2. Add this to your `.gitlab-ci.yml` file for each template:

  ```yaml
  include:
    - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/${TEMPLATE_NAME}.yml'
  ```

3. :tada:

## Scanners

The following security scanners and linters are included:

- [checkov](https://github.com/bridgecrewio/checkov): A static code analysis tool for infrastructure-as-code, providing security and compliance scanning for Terraform, Kubernetes, Docker, and more.
- [depscan](https://github.com/AppThreat/dep-scan): Security audit for projects and container images based on known vulnerabilities and advisories for project dependencies.
- [dependencycheck](https://github.com/jeremylong/DependencyCheck): OWASP dependency-check is a software composition analysis utility that detects publicly disclosed vulnerabilities in application dependencies.
- [detect-secrets](https://github.com/Yelp/detect-secrets): A tool from Yelp designed to detecting and preventing secrets in code.
- [dlint](https://github.com/dlint-py/dlint): A tool for promoting Python best practices and preventing common coding errors through static analysis.
- [earlybird](https://github.com/americanexpress/earlybird): EarlyBird is a sensitive data detection tool capable of scanning source code repositories for clear text password violations, PII, outdated cryptography methods, key files and more.
- [gitleaks](https://github.com/gitleaks/gitleaks): A secret detection scanner tool that detects hardcoded secrets like passwords, tokens, and API keys in git repos.
- [git-secrets](https://github.com/awslabs/git-secrets): A secret detection scanning tool from AWS labs.
- [gosec](https://github.com/securego/gosec): Gosec is a security checker for Golang, inspecting source code for security problems by scanning the AST.
- [go-staticcheck](https://github.com/dominikh/go-tools): Staticcheck is a comprehensive Go linter that checks for bugs, performance issues, and incorrect coding practices.
- [govulncheck](https://go.dev/blog/vuln): `govulncheck` is a low-noise, reliable way for Go users to learn about known vulnerabilities that may affect their projects.
- [grype](https://github.com/anchore/grype): Grype is a vulnerability scanner for container images and filesystems, providing a bill of materials (BOM) and matching against known vulnerabilities.
- [lockfile-lint](https://github.com/lirantal/lockfile-lint): A tool for linting lockfiles to ensure they adhere to pre-defined security policies, mitigating potential attack vectors.
- [megalinter](https://github.com/oxsecurity/megalinter): Analyzes and lints 50 languages, 22 formats, 21 tooling formats for possible security problems and general issues.
- [OSSGadget](https://github.com/microsoft/OSSGadget): A collection of tools for analyzing open source packages, offering capabilities like locating source code, performing basic analyses, and estimating package health.
- [osv-scanner](https://github.com/google/osv-scanner): A vulnerability scanner from Google connects a project's list of dependencies with the vulnerabilities that affect them.
- [Scancode](https://github.com/nexB/scancode-toolkit): ScanCode scans code to inventory and detects dependencies, licenses, and copyrights for open source and third-party packages.
- [Scorecard](https://github.com/ossf/scorecard): An open-source tool that automatically assesses the security posture of open-source projects using a variety of metrics.
- [Terrascan](https://github.com/tenable/terrascan): A static code analyzer for Infrastructure as Code, detecting security vulnerabilities and compliance violations, and mitigating risks before provisioning cloud native infrastructure.
- [tfsec](https://github.com/aquasecurity/tfsec): A security scanner for HashiCorp Terraform scripts, providing actionable, context-aware recommendations to enhance security.
- [trivy](https://github.com/aquasecurity/trivy): Comprehensive vulnerability scanner for containers and filesystems, supporting multiple formats and sources.
- [truffleHog](https://github.com/trufflesecurity/truffleHog): Searches through git repositories for secrets, digging deep into commit history and branches.
- [xeol](https://github.com/xeol-io/xeol): An End Of Life (EOL) package scannner that works for container images, repositories, and SBOMs.

## Example

Comment out any scanners that are not applicable or that you don't want to use:

```yml
include:
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/bandit.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/brakeman.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/checkov.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/dependency-check.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/depscan.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/detect-secrets.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/dlint.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/gitleaks.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/git-secrets.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/gosec.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/go-staticcheck.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/go-vet.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/govulncheck.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/guarddog.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/grype.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/lockfile-lint.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/mega-linter.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/mega-linter-security.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/noir.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/oss-gadget.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/osv-scanner.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/scancode.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/scorecard.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/secrets.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/semgrep.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/super-linter.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/terrascan.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/tfsec.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/trivy.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/trufflehog.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/xeol.yml'
```

## Greg's Choice :chefkiss:

```yml
include:
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/checkov.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/depscan.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/guarddog.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/mega-linter-security.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/scorecard.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/semgrep.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/osv-scanner.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/trivy.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/trufflehog.yml'
  - 'https://gitlab.com/greg/sec-ci/-/raw/main/ci/xeol.yml'
```

## Contributing

Contributions are welcome! Please feel free to submit a merge request or open an issue.
